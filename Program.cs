﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            Console.OutputEncoding = Encoding.UTF8;

            //GetTotalTax
            Console.WriteLine("Hello World!");
            Console.WriteLine("Please enter companies number");
            string companiesNumber = Console.ReadLine();
            Console.WriteLine($"There are {companiesNumber} companies!");

            //GetCongratulation
            Console.WriteLine("Please enter age:");
            string age = Console.ReadLine();
            int.TryParse(age, out int ageInt);
            Console.WriteLine(program.GetCongratulation(ageInt));

            //GetMultipliedNumbers
            Console.WriteLine("Please enter first number:");
            string firstNumber = Console.ReadLine();
            Console.WriteLine("Please enter second number:");
            string secondNumber = Console.ReadLine();
            double result = program.GetMultipliedNumbers(firstNumber, secondNumber);
            Console.WriteLine($"Your result is: {result}");

            //GetFigureValues
            Dimensions dimensions = new Dimensions();
            Console.WriteLine("Please choose what you want to calculate: Square or Perimeter");
            Enum.TryParse(Console.ReadLine(), out Parameters calculation);
            Console.WriteLine("Please choose figure: Triangle, Rectangle, Circle");
            Enum.TryParse(Console.ReadLine(), out Figures figure);
            switch (figure)
            {
                case Figures.Triangle:
                    Console.WriteLine("Please enter first side:");
                    dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Please enter second side:");
                    dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Please enter third side:");
                    dimensions.ThirdSide = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Please enter height:");
                    dimensions.Height = Convert.ToDouble(Console.ReadLine());
                    break;
                case Figures.Rectangle:
                    Console.WriteLine("Please enter first side:");
                    dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Please enter second side:");
                    dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
                    break;
                case Figures.Circle:
                    Console.WriteLine("Please enter radius:");
                    dimensions.Radius = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Please enter diameter");
                    dimensions.Diameter = Convert.ToDouble(Console.ReadLine());
                    break;
            }
            Console.WriteLine($"The result of your calculation is: " +
                $"{ Math.Round(program.GetFigureValues(figure, calculation, dimensions), MidpointRounding.AwayFromZero)}");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * companyRevenue * tax / 100;
        }

        public string GetCongratulation(int input)
        {
            string congratulation = "";

            if (input % 2 == 0 && input >= 18)
            {
                congratulation = "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                congratulation = "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                congratulation = $"Поздравляю с {input}-летием!";
            }

            return congratulation;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double result = 0;
            var culture = CultureInfo.GetCultureInfo("fr-FR");
            if (!double.TryParse(first, NumberStyles.Any, culture, out double firstNumber) ||
                !double.TryParse(second, NumberStyles.Any, culture, out double secondNumber))
            {
                Console.WriteLine("Please make sure you've entered valid numbers!");
                return result;
            }
            else
            {
                result = firstNumber * secondNumber;
                return result;
            }
        }

        public double GetFigureValues(Figures figureType, Parameters parameterToCompute, Dimensions dimensions)
        {
            double result = 0;

            switch (figureType)
            {
                case Figures.Triangle:
                    result = GetTriangleCalculation(parameterToCompute, dimensions);
                    break;
                case Figures.Rectangle:
                    result = GetRectangleCalculation(parameterToCompute, dimensions);
                    break;
                case Figures.Circle:
                    result = GetCircleCalculation(parameterToCompute, dimensions);
                    break;
            }
            return Math.Round(result, MidpointRounding.AwayFromZero);
        }

        private double GetCircleCalculation(Parameters parameterToCompute, Dimensions dimensions)
        {
            if (parameterToCompute == Parameters.Square)
            {
                return (3.14) * dimensions.Radius * dimensions.Radius;
            }
            else
            {
                return dimensions.Radius * 2 * 3.14;
            }
        }

        private double GetRectangleCalculation(Parameters parameterToCompute, Dimensions dimensions)
        {
            if (parameterToCompute == Parameters.Square)
            {
                return dimensions.FirstSide * dimensions.SecondSide;
            }
            else
            {
                return (dimensions.FirstSide + dimensions.SecondSide) * 2;
            }
        }

        public double GetTriangleCalculation(Parameters parameterToCompute, Dimensions dimensions)
        {
            if (parameterToCompute == Parameters.Square)
            {
                if (dimensions.Height != 0)
                {
                    return dimensions.FirstSide * dimensions.Height / 2;
                }
                else
                {
                    double s = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                    return Math.Sqrt(s * (s - dimensions.FirstSide) * (s - dimensions.SecondSide) * (s - dimensions.ThirdSide));
                }
            }
            else
            {
                return dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
            }
        }
    }
}
